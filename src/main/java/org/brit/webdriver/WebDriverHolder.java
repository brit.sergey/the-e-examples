package org.brit.webdriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author SerhiiBryt
 **/
public class WebDriverHolder {

    private WebDriver driver;

    private static WebDriverHolder instance = null;

    private WebDriverHolder() {
    }

    public WebDriver getDriver() {
        return driver;
    }

    public JavascriptExecutor getJavascriptExecutor() {
        return (JavascriptExecutor) driver;
    }

    public WebDriverWait getWaiter(){
        return new WebDriverWait(driver, 30);
    }

    public static WebDriverHolder getInstance() {
        if (instance == null) {
            instance = new WebDriverHolder();
        }
        return instance;
    }

    public void initDriver(WebDriverType webDriverType) {
        driver = WebDriverFactory.getDriver(webDriverType);
        driver.manage().window().maximize();
    }
}
