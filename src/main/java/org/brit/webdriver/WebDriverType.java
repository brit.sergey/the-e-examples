package org.brit.webdriver;

/**
 * @author SerhiiBryt
 **/
public enum WebDriverType {
    CHROME_DRIVER,
    CHROME_DRIVER_FOE_DOWNLOADS,
    FIREFOX_DRIVER,
    REMOTE_DRIVER
}
