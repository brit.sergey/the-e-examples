package org.brit.webdriver;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.brit.constants.ApplicationConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @author SerhiiBryt
 **/
public class WebDriverFactory {
    public static WebDriver getDriver(WebDriverType webDriverType) {
        WebDriver driver = null;
        switch (webDriverType) {
            case CHROME_DRIVER:
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case FIREFOX_DRIVER:
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            case CHROME_DRIVER_FOE_DOWNLOADS:
                try {
                    FileUtils.cleanDirectory(ApplicationConstants.downloadsFolder);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                WebDriverManager.chromedriver().setup();
                Map<String, Object> prefsMap = new HashMap<String, Object>();
                prefsMap.put("profile.default_content_settings.popups", 0);
                prefsMap.put("download.default_directory",
                        ApplicationConstants.downloadsFolder.getAbsolutePath());

                ChromeOptions option = new ChromeOptions();
                option.setExperimentalOption("prefs", prefsMap);
                option.addArguments("--test-type");
                option.addArguments("--disable-extensions");

                driver = new ChromeDriver(option);
                break;
            case REMOTE_DRIVER:
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setBrowserName("chrome");
                capabilities.setVersion("85.0");
                capabilities.setCapability("enableVNC", true);
                capabilities.setCapability("enableVideo", false);

                try {
                    driver = new RemoteWebDriver(
                            URI.create("http://192.168.31.198:4444/wd/hub").toURL(),
                            capabilities
                    );
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

        }
        WebDriverRunner.setWebDriver(driver);
        Configuration.downloadsFolder = "downloads";
        driver.manage().window().maximize();
        return driver;
    }
}
