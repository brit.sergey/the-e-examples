package org.brit.constants;

import java.io.File;

/**
 * @author SerhiiBryt
 **/
public class ApplicationConstants {
    public static final String APP_URL = "https://the-internet.herokuapp.com";
    public static final String ALERT_PAGE = APP_URL + "/javascript_alerts";
    public static final String WINDOWS_PAGE = APP_URL + "/windows";
    public static final String LOGIN_FORM = APP_URL + "/login";
    public static final String UPLOAD_FILE = APP_URL + "/upload";
    public static final String DOWNLOAD_FILE = APP_URL + "/download";
    public static final String JS_MENU = APP_URL + "/jqueryui/menu";

    public static final File downloadsFolder = new File("downloads");
}
