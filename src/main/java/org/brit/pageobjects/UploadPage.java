package org.brit.pageobjects;

//import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.brit.pageobjects.login_logout.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @author SerhiiBryt
 **/
public class UploadPage extends BasePage {
    static final Logger logger = LoggerFactory.getLogger(LoginPage.class);

    @FindBy(id = "file-upload")
    private WebElement fileUploadComponent;

    @FindBy(id = "file-submit")
    private WebElement uploadSubmitButton;

    @FindBy(id = "uploaded-files")
    private WebElement messageBox;



  //  @Step("Choose file for upload {fileToUpload}")
    public UploadPage chooseFileForUpload(File fileToUpload) {
        logger.info("Choose file with name {} for upload", fileToUpload.getName());
        fileUploadComponent.sendKeys(fileToUpload.getAbsolutePath());
        return this;
    }

    public UploadPage submitUpload() {
        logger.info("Submit upload");
        uploadSubmitButton.click();
        return new UploadPage();
    }

    //@Step("Upload file {fileToUpload}")
    public UploadPage uploadFile(File fileToUpload) {
        logger.info("Upload file");
        return chooseFileForUpload(fileToUpload)
                .submitUpload();
    }

    //@Step("Get text from message")
    public String getMessageBoxText() {
        logger.info("Getting text from message");
        return messageBox.getText();
    }
}
