package org.brit.pageobjects;

import com.codeborne.selenide.WebDriverRunner;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author SerhiiBryt
 **/
public class BasePage {

    @FindBy(xpath = "//div[@id='content']//h3")
    protected WebElement messageHeader;

    public String getMessageHeaderText() {
        return messageHeader.getText();
    }

    public BasePage(WebDriver webDriver) {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }

    public BasePage() {
        PageFactory.initElements(WebDriverRunner.getWebDriver(), this);
    }

    public void sleep(long msecs) {
        try {
            Thread.sleep(msecs);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void waitTillElementIsVisible(WebElement element) {
        WebDriverHolder.getInstance().getWaiter()
                .until(ExpectedConditions.visibilityOf(element));

    }

    public BasePage waitTillPageLoaded() {
        return this;
    }

}
