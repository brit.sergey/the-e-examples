package org.brit.pageobjects.jsmenu;

/**
 * @author SerhiiBryt
 **/
public enum MenuThirdLevelLevel {
    PDF,
    CSV,
    Excel
}
