package org.brit.pageobjects.jsmenu;

import org.apache.commons.io.FileUtils;
import org.brit.pageobjects.BasePage;

import java.io.File;
import java.io.FileNotFoundException;

import static com.codeborne.selenide.Selectors.byLinkText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.actions;

/**
 * @author SerhiiBryt
 **/
public class JSMenuPageObject extends BasePage {

    public JSMenuPageObject selectMenu(MenuFirstLevel menuFirstLevel, MenuSecondLevel... menuSecondLevel) {
        actions().moveToElement($(byLinkText(menuFirstLevel.name()))).build().perform();
        if (menuSecondLevel.length > 0)
            actions().moveToElement($(byLinkText(menuSecondLevel[0].name()
                    .replaceAll("_", " ")))).build().perform();
        return this;
    }

    public File downloadFile(MenuThirdLevelLevel menuThirdLevelLevel) {
        try {
            return $(byLinkText(menuThirdLevelLevel.name())).download();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public File downloadCSV() {
        return selectMenu(MenuFirstLevel.Enabled, MenuSecondLevel.Downloads)
                .downloadFile(MenuThirdLevelLevel.CSV);
    }


}
