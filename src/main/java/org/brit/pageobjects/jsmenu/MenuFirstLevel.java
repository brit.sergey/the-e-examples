package org.brit.pageobjects.jsmenu;

/**
 * @author SerhiiBryt
 **/
public enum MenuFirstLevel {
    Disabled,
    Enabled
}
