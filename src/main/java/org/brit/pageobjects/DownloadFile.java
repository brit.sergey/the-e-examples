package org.brit.pageobjects;

//import io.qameta.allure.Step;

import org.brit.constants.ApplicationConstants;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.File;

/**
 * @author SerhiiBryt
 **/
public class DownloadFile extends BasePage {

    //  @Step("Download file with name {fileName}")
    public File downloadFile(String fileName) {
        WebDriverHolder.getInstance().getDriver().findElement(By.linkText(fileName)).click();
        sleep(5000);
        return new File(ApplicationConstants.downloadsFolder, fileName);
    }
}
