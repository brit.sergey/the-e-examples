package org.brit.pageobjects.login_logout;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.brit.pageobjects.BasePage;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selenide.$;


/**
 * @author SerhiiBryt
 **/
public class LoginPage extends BasePage {
    static final Logger logger = LoggerFactory.getLogger(LoginPage.class);


    private SelenideElement userNameInput = $("#username");
    private SelenideElement passwordInput = $("#password");

    @FindBy(xpath = "//button")
    private WebElement loginButton;

    private SelenideElement errorMessage = $(".flash.error");

    @FindBy(css = ".flash.success")
    private WebElement successfulMessage;

    public LoginPage() {
      //  super();
        logger.info("Init " + LoginPage.class.getName() + " page");
    }

    @Override
    @Step("Wait till page is loaded")
    public LoginPage waitTillPageLoaded() {
        logger.info("Wait till page is loaded...");
        WebDriverHolder.getInstance().getWaiter()
                .until(ExpectedConditions.visibilityOfAllElements(userNameInput, passwordInput));
        return new LoginPage();
    }

    @Step("Login with username: {userName} and password {password}")
    public PageAfterLogin loginSuccessful(String userName, String password) {
        logger.info("Login in with parameters:\nUser: "
                + userName + "\nPass: " + password);
        userNameInput.setValue(userName);
        passwordInput.setValue(password).pressEnter();
        return new PageAfterLogin();
    }

    @Step("Login with username: {userName} and password {password} and return to {pageToReturn}")
    public <T extends BasePage> T login(String userName, String password, Class<T> pageToReturn) {
        logger.info("Login in with parameters:\nUser: "
                + userName + "\nPass: " + password);
        userNameInput.setValue(userName);
        passwordInput.setValue(password).pressEnter();
        try {
            return pageToReturn.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public LoginPage loginUnSuccessful(String userName, String password) {
        userNameInput.setValue(userName);
        passwordInput.setValue(password).pressEnter();
        return new LoginPage();
    }

    @Step("Check if error message is visible")
    public boolean isErrorMessageVisible() {
        logger.info("Check is errorMessage visible!");
        return errorMessage.is(Condition.visible);
    }

    @Step("Check if successful message is visible")
    public boolean isSuccessfulMessageVisible() {
        try {
            logger.info("Check is successfulMessage visible!");
            return successfulMessage.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    @Step("Getting successful message text")
    public String getSuccessfulMessageText() {
        logger.info("Get text from message");
        return successfulMessage.getText();
    }

    @Step("Getting error message text")
    public String getErrorMessageText() {
        logger.info("Get text from message");
        return errorMessage.getText();
    }


}
