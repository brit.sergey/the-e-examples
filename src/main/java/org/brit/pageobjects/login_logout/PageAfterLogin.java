package org.brit.pageobjects.login_logout;

import io.qameta.allure.Step;
import org.brit.pageobjects.BasePage;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author SerhiiBryt
 **/
public class PageAfterLogin extends BasePage {
    static final Logger logger = LoggerFactory.getLogger(PageAfterLogin.class);
    @FindBy(css = ".flash.success")
    private WebElement successfulMessage;

    @FindBy(css = ".button.secondary.radius")
    private WebElement logoutButton;

    public PageAfterLogin() {
        super();
        logger.info("Init " + LoginPage.class.getName() + " page");
    }

    @Override
    @Step("Wait till page is loaded")
    public PageAfterLogin waitTillPageLoaded() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        new WebDriverWait(driver, 30)
                .until(ExpectedConditions.visibilityOfAllElements(logoutButton, successfulMessage));
        return new PageAfterLogin();
    }

    @Step("Check if successful message is visible")
    public boolean isSuccessfulMessageVisible() {
        try {
            logger.info("Check is successfulMessage visible!");
            return successfulMessage.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    @Step("Logout")
    public LoginPage logout() {
        logger.info("Logging out...");
        waitTillElementIsVisible(logoutButton);
        logoutButton.click();
        return new LoginPage();
    }
}
