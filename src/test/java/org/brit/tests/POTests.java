package org.brit.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Issue;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.brit.base.BaseTest;
import org.brit.constants.ApplicationConstants;
import org.brit.pageobjects.login_logout.LoginPage;
import org.brit.pageobjects.login_logout.PageAfterLogin;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author SerhiiBryt
 **/
public class POTests extends BaseTest {
    String user = "tomsmith";
    String pass = "SuperSecretPassword!";

    @BeforeClass
    public void beforeClass111() {
        goToUrl(ApplicationConstants.LOGIN_FORM);
    }

    @Test
    @Description("This is successful login test")
    @Severity(SeverityLevel.CRITICAL)
    public void test() {
        PageAfterLogin pageAfterLogin =
                new LoginPage()
                        .login(user, pass, PageAfterLogin.class)
                        .waitTillPageLoaded();
        Assert.assertTrue(pageAfterLogin.isSuccessfulMessageVisible());
        LoginPage logout = pageAfterLogin.logout();

        Assert.assertTrue(logout.isSuccessfulMessageVisible());
        Assert.assertTrue(logout.getSuccessfulMessageText()
                .contains("You logged out of the secure area!"));
    }

    @Test
    @Description("This is unsuccessful login test")
    @Severity(SeverityLevel.TRIVIAL)
    public void testUnsuccessfulLogin() {
        LoginPage loginPage =
                new LoginPage()
                        .login(user, pass + "55", LoginPage.class)
                        .waitTillPageLoaded();
        Assert.assertTrue(loginPage.isErrorMessageVisible());
        Assert.assertTrue(loginPage.getErrorMessageText().contains("   Your password is invalid!  asd  "));
    }

}
