package org.brit.tests;

//import io.qameta.allure.Description;
//import io.qameta.allure.Issue;
//import io.qameta.allure.Severity;
//import io.qameta.allure.SeverityLevel;
import org.apache.commons.io.FileUtils;
import org.brit.base.BaseTest;
import org.brit.constants.ApplicationConstants;
import org.brit.pageobjects.DownloadFile;
import org.brit.pageobjects.UploadPage;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author SerhiiBryt
 **/
public class UploadDownloadTests extends BaseTest {
    private File fileToUpload = new File("data.txt");

    @Test(priority = 5)
    //  @Description("Upload Test")
    //  @Severity(SeverityLevel.CRITICAL)
    public void uploadTest() {
        goToUrl(ApplicationConstants.UPLOAD_FILE);
        String messageBoxText = new UploadPage()
                .uploadFile(fileToUpload)
                .getMessageBoxText();
        Assert.assertEquals(fileToUpload.getName(), messageBoxText.trim());
    }

    @Test(priority = 100)
    // @Description("Download Test")
    // @Issue("555")
    public void downloadTest() throws IOException {
        goToUrl(ApplicationConstants.UPLOAD_FILE);
        new UploadPage().uploadFile(fileToUpload);

        goToUrl(ApplicationConstants.DOWNLOAD_FILE);
        File file = new DownloadFile().downloadFile(fileToUpload.getName());

        List<String> from1 = FileUtils.readLines(fileToUpload, "UTF-8");
        List<String> from2 = FileUtils.readLines(file, "UTF-8");

        Assert.assertNotEquals(from1, from2, "Message in assert");
    }

}
