package org.brit.tests;

import com.opencsv.bean.OpencsvUtils;
import org.apache.commons.io.FileUtils;
import org.brit.base.BaseTest;
import org.brit.constants.ApplicationConstants;
import org.brit.pageobjects.jsmenu.JSMenuPageObject;
import org.brit.pageobjects.jsmenu.MenuFirstLevel;
import org.brit.pageobjects.jsmenu.MenuSecondLevel;
import org.brit.pageobjects.jsmenu.MenuThirdLevelLevel;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static com.codeborne.selenide.Selectors.byLinkText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.actions;

/**
 * @author SerhiiBryt
 **/
public class JSMenuTests extends BaseTest {
    @BeforeClass
    public void beforeJSMenuTestsClass() {
        goToUrl(ApplicationConstants.JS_MENU);
    }

    @Test
    public void jsMenuTest() throws IOException {
        File csv = new JSMenuPageObject().downloadCSV();


        FileUtils
                .readLines(csv, "UTF-8")
                .forEach(System.out::println);

    }
}
