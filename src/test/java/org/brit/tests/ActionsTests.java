package org.brit.tests;


import org.brit.base.BaseTest;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.Duration;

/**
 * @author SerhiiBryt
 **/
public class ActionsTests extends BaseTest {
    WebDriver driver = WebDriverHolder.getInstance().getDriver();

    @BeforeClass
    public void beforeAlertTestsClass() {
        goToUrl("https://material.angular.io/cdk/drag-drop/examples");
    }

    @Test
    public void DragAndDropTest(){
        WebElement element = driver.findElement(By.xpath("//div[contains(text(),'Dragging starts after one second')]"));
        new Actions(driver)
                .moveToElement(element)
                .pause(Duration.ofSeconds(2))
                .clickAndHold(element)
                .pause(Duration.ofSeconds(2))
                .moveByOffset(50, 50)
                .pause(Duration.ofSeconds(5))
                .release();
    }

}
