package org.brit.tests;

import org.brit.MyListener;
import org.brit.base.BaseTest;
import org.brit.constants.ApplicationConstants;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

/**
 * @author SerhiiBryt
 **/
@Listeners(MyListener.class)
public class AlertTests extends BaseTest {

    By context = By.id("content");
    By result = By.id("result");


    @BeforeClass
    public void beforeAlertTestsClass() {
        goToUrl(ApplicationConstants.ALERT_PAGE);
    }

    @Test
    public void clickJSAlertTest() {
        WebElement button = WebDriverHolder.getInstance().getDriver().findElement(context)
                .findElement(By.xpath(".//button[text()='Click for JS Alert']"));
        button.click();
        Alert alert = WebDriverHolder.getInstance().getDriver().switchTo().alert();
        String text = alert.getText();
        alert.accept();
        Assert.assertEquals(text, "I    am a JS Alert");
        WebDriverHolder.getInstance().getDriver().switchTo().defaultContent();
        Assert.assertEquals(WebDriverHolder.getInstance().getDriver()
                .findElement(result).getText(), "You successfuly clicked an alert");
    }

    @Test
    public void clickJSAlertWithJSTest() {
        String script = "return jsAlert()";
        WebDriverHolder.getInstance().getJavascriptExecutor().executeScript(script);
        Alert alert = WebDriverHolder.getInstance().getDriver().switchTo().alert();
        Assert.assertEquals(alert.getText(), "I am a JS Alert");
        alert.accept();
        WebDriverHolder.getInstance().getDriver().switchTo().defaultContent();
        Assert.assertEquals(WebDriverHolder.getInstance().getDriver()
                .findElement(result).getText(), "You successfuly clicked an alert");

    }

    @Test(enabled = false)
    public void basicAuthTest() {
        goToUrl("https://the-internet.herokuapp.com/basic_auth");
        Assert.assertEquals(WebDriverHolder.getInstance()
                .getDriver().findElement(By.xpath("//h3")).getText(), "Basic Auth");
    }
}
