package org.brit.tests;

import org.brit.base.BaseTest;
import org.brit.constants.ApplicationConstants;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author SerhiiBryt
 **/
public class WindowsTests extends BaseTest {
    WebDriver driver = WebDriverHolder.getInstance().getDriver();

    @BeforeClass
    public void beforeWindowsTestsClass() {
        goToUrl(ApplicationConstants.WINDOWS_PAGE);
    }

    @Test
    public void switchToWindow() {
        driver.findElement(By.linkText("Click Here")).click();
        Object[] objects = driver.getWindowHandles().toArray();
        driver.switchTo().window(objects[1].toString());
        Assert.assertEquals(driver.findElement(By.tagName("h3")).getText(), "New Window");
        driver.close();
        driver.switchTo().window(objects[0].toString());
        Assert.assertEquals(driver.findElement(By.tagName("h3")).getText(), "Opening a new window");
    }
}
