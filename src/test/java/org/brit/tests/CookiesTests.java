package org.brit.tests;

import org.brit.base.BaseTest;
import org.brit.constants.ApplicationConstants;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author SerhiiBryt
 **/
public class CookiesTests extends BaseTest {
    WebDriver driver = WebDriverHolder.getInstance().getDriver();
    @BeforeClass
    public void beforeCookiesTestsClass() {
        goToUrl(ApplicationConstants.LOGIN_FORM);
    }

    @Test
    public void getCookiesTest() {
        String cookieName = "rack.session";
        String cookieValue = "";

        driver.findElement(By.id("username")).sendKeys("tomsmith");
        driver.findElement(By.id("password")).sendKeys("SuperSecretPassword!");
        driver.findElement(By.id("password")).submit();

        cookieValue = driver.manage().getCookieNamed(cookieName).getValue();

        driver.manage().deleteCookieNamed(cookieName);

        driver.navigate().refresh();

        WebElement element = driver.findElement(By.cssSelector(".flash.error"));
        Assert.assertTrue(element.getText().contains("You must login to view the secure area!"));

        driver.manage().addCookie(new Cookie(cookieName, cookieValue));

        driver.navigate().refresh();

        Assert.assertTrue(driver.findElement(By.cssSelector(".button.secondary.radius")).isDisplayed());
//        for (Cookie cookie : driver.manage().getCookies()) {
//            System.out.println(cookie.getName() + " ==> " + cookie.getValue());
//        }
//
//        driver.manage().deleteAllCookies();
//
//        for (Cookie cookie : driver.manage().getCookies()) {
//            System.out.println(cookie.getName() + " ==> " + cookie.getValue());
//        }
//
//        driver.manage().addCookie(new Cookie("myCookie", "MyCookieValue"));
//
//        System.out.println(driver.manage().getCookieNamed("myCookie").getValue());
//
//        driver.findElement(By.cssSelector(".button.secondary.radius")).click();
    }
}
