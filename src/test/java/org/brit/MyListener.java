package org.brit;

//import io.qameta.allure.Attachment;

import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.brit.webdriver.WebDriverHolder;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener;

import java.io.File;
import java.io.IOException;


/**
 * @author SerhiiBryt
 **/
public class MyListener implements IResultListener {
    static final Logger logger = LoggerFactory.getLogger(MyListener.class);

    @Override
    public void onConfigurationSuccess(ITestResult itr) {

    }

    @Override
    public void onConfigurationFailure(ITestResult itr) {

    }

    @Override
    public void onConfigurationSkip(ITestResult itr) {

    }

    @Override
    public void onTestStart(ITestResult result) {
        logger.info(result.getName() + " Started!!!");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info(result.getName() + " Finished SUCCESSFULLY!!!");
    }



    @Override
    public void onTestFailure(ITestResult result) {
        logger.info(result.getName() + " FAILED!!!!");
        try {
            getScreenShot(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }

    private File takeScreenshot(String testName) {
        File screenShot = ((TakesScreenshot) WebDriverHolder.getInstance().getDriver())
                .getScreenshotAs(OutputType.FILE);
        File distFile = new File(new File("screens"), testName + ".png");
        try {
            FileUtils.copyFile(screenShot, distFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return distFile;
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    private byte[] getScreenShot(ITestResult iTestResult) throws IOException {
        return FileUtils.readFileToByteArray(takeScreenshot(iTestResult.getName()));
    }
}
