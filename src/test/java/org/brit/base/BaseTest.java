package org.brit.base;

//import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.brit.MyExtentReportListener;
import org.brit.MyListener;
import org.brit.webdriver.WebDriverHolder;
import org.brit.webdriver.WebDriverType;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.uncommons.reportng.HTMLReporter;
import org.uncommons.reportng.JUnitXMLReporter;

import java.io.File;
import java.io.IOException;

import static com.codeborne.selenide.Selenide.open;

/**
 * @author SerhiiBryt
 **/
@Listeners({HTMLReporter.class, JUnitXMLReporter.class , MyExtentReportListener.class, MyListener.class})
public class BaseTest {
    //  protected static WebDriver driver;


    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() throws IOException {
//        System.setProperty("extent.reporter.html.start", "true");
//        System.setProperty("extent.reporter.html.config", "html-config.xml");
//        System.setProperty("extent.reporter.html.out", "test-output/HtmlReport/ExtentHtml.html");
        //   driver = WebDriverFactory.getDriver(WebDriverType.CHROME_DRIVER);
        String browser = System.getProperty("browser", "CHROME_DRIVER");
        WebDriverHolder.getInstance().initDriver(WebDriverType.valueOf(browser.toUpperCase()));
        FileUtils.forceMkdir(new File("screens"));
        FileUtils.cleanDirectory(new File("screens"));
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        if (WebDriverHolder.getInstance().getDriver() != null) {
            WebDriverHolder.getInstance().getDriver().quit();
        }
    }

   // @Step("GO to {url}")
    public void goToUrl(String url) {
        open(url);
    }


}
